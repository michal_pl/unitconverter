package com.michal;

import com.michal.data.source.TestCategoriesProvider;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        String categoriesFilePath = "/home/michal/IdeaProjects/UnitConverter/resources/example.categories";
//        UnitConverter unitConverter = new UnitConverter(new CategoriesFileReader(categoriesFilePath).get());
        UnitConverter unitConverter = new UnitConverter(new TestCategoriesProvider().get());
        unitConverter.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        unitConverter.setVisible(true);
    }
}
