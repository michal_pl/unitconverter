package com.michal.data;

public class Unit {
    private String name;
    private String conversionRate; // jako string, bo w pliku mamy stringi, konwersja przy obliczaniu

    public Unit(String name, String conversionRate) {
        this.name = name;
        this.conversionRate = conversionRate;
    }

    // Nie robimy setterów, nie ma takiej potrzeby, obiekty będą immutable (nieedytowalne)

    public String getName() {
        return name;
    }

    public String getConversionRate() {
        return conversionRate;
    }
}
