package com.michal;


import com.michal.data.Unit;
import com.michal.data.Category;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class UnitConverter extends JFrame implements ActionListener {
    private JComboBox<String> categorySelector = new JComboBox<>();
    private JComboBox<String> leftUnitSelector = new JComboBox<>();
    private JComboBox<String> rightUnitSelector = new JComboBox<>();
    private JButton convertButton = new JButton("Convert");
    private double leftValue;
    private double rightValue;

    private List<Category> categories;

    public UnitConverter(List<Category> categories) {
        setSize(500, 400);
        setTitle("Unit Converter");
        setLayout(null);

        this.categories = categories;

        categorySelector.setBounds(30, 40, 400, 30);
        add(categorySelector);
        for (Category category : categories) {
            categorySelector.addItem(category.getName());
        }
        categorySelector.addActionListener(this);


        leftUnitSelector.setBounds(30, 100, 180, 30);
        add(leftUnitSelector);

        rightUnitSelector.setBounds(250, 100, 180, 30);
        add(rightUnitSelector);

        JSpinner leftValueSelector = new JSpinner();
        leftValueSelector.setBounds(60, 160, 100, 60);
        add(leftValueSelector);

        JSpinner rightValueSelector = new JSpinner();
        rightValueSelector.setBounds(280, 160, 100, 60);
        add(rightValueSelector);

        convertButton.setBounds(30, 260, 400, 50);
        add(convertButton);
        convertButton.addActionListener(e -> JOptionPane.showMessageDialog(null, "Nothing to do... yet :)"));

        JSeparator separator = new JSeparator();
        separator.setBounds(0, 85, 500, 1);
        add(separator);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String categoryName = categorySelector.getSelectedItem()
                                              .toString();

        Category category = categories.stream()
                                      .filter(c -> c.getName()
                                                    .equals(categoryName))
                                      .findFirst()
                                      .orElseThrow(() -> new RuntimeException("No such category " + categoryName));

        leftUnitSelector.removeAllItems();
        rightUnitSelector.removeAllItems();

        for (Unit unit : category.getUnits()) {
            leftUnitSelector.addItem(unit.getName());
            rightUnitSelector.addItem(unit.getName());
        }
    }
}
